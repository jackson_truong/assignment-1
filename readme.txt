Name: Jackson Truong
Student ID: 100933745

What Does Work and has been completed:
- All parts in the game logic marking scheme has been completed. 
- All Require TDD test cases has been completed for the Rules Engine
- Only Steps 1 - 2 have been completed for the networking part
	- Meaning a client can connect to the server
	- The server can use log4j to log occurrences
	- The server can send messages to the client

What doesn't work
- Multithread servers has not been implemented
- The game itself cannot be played
- Game is missing the networking part that connects all the logic in rules Engine
- There is no test plans completed

Networking Solution
- Based on opening a server socket, and opening a client socket
- Clients can connect through the client socket
- Currently no multithreading capabilities

TDD RulesEngineTestSuite
- Contains all the TDD tests for the ruleEngines tests.

TDD ServerClientTestSuite
- Contain all the completed TDD tests for client tests.

Bit Bucket ID
- jackson_truong
- I have already invited/added "hneedham" to the private repository



