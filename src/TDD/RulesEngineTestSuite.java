package TDD;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
	TestRulesEngineSelect.class,
	TestRulesEngineAttackSpeed.class,
	TestRulesEngineTarget.class,
	TestRulesEngineAttackDirection.class,
	TestRulesEngineAttackConclusion.class,
	TestRulesEngineTwoPlayers.class,
	TestRulesEngineThreePlayers.class,
	TestRulesEngineFourPlayers.class
})

public class RulesEngineTestSuite {

}
