package TDD;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import RulesEngine.Player;
import RulesEngine.RulesEngine;

public class TestRulesEngineThreePlayers {
	RulesEngine rEngine;
	
	@BeforeClass
    public static void BeforeClass() {
        System.out.println("@BeforeClass: TestRulesEngineThreePlayers");
    }
    
    @Before
    public void setUp() {
		System.out.println("@Before: TestRulesEngineThreePlayers");
		rEngine = new RulesEngine();
	}
	
    @After
    public void tearDown () {
		System.out.println("@After(): TestRulesEngineThreePlayers");
		rEngine = null;
	}
	
    @AfterClass
    public static void afterClass () {
    	 System.out.println("@AfterClass: TestRulesEngineThreePlayers");
    }
	
    @Test
	public void GameRound3PlayersDiffTargetNoHits() {
		System.out.println("@Test(): GameRound3PlayersDiffTargetNoHits");
		rEngine.setNumberPlayers(3);
		Player P1 = rEngine.addPlayers("Jackson", "100.000.0.0");
		Player P2 = rEngine.addPlayers("Truong", "100.000.0.0");
		Player P3 = rEngine.addPlayers("Robert", "100.000.0.0");
		P1.setCommands("Thrust", 2, "Dodge", 2);
		P2.setCommands("Swing", 2, "Duck", 2);
		P3.setCommands("Smash", 2, "Charge", 2);
		Boolean result1 = rEngine.AttackConclusion(P1,P2); //AttackConclusion(PlayerAtk, PlayerDef)
		Boolean result2 = rEngine.AttackConclusion(P2,P3); //AttackConclusion(PlayerAtk, PlayerDef)
		Boolean result3 = rEngine.AttackConclusion(P3,P1); //AttackConclusion(PlayerAtk, PlayerDef)
		assertTrue(result1 == false && P2.getCountWounds() == 0); //No hits done by player one attacking player two
		assertTrue(result2 == false && P3.getCountWounds() == 0); //No hits done by player two attacking player three
		assertTrue(result3 == false && P1.getCountWounds() == 0); //No hits done by player three attacking player one
		System.out.println(rEngine.EndRoundMessage());;
	}
    
    @Test
	public void GameRound3PlayersDiffTargetOneHits() {
		System.out.println("@Test(): GameRound3PlayersDiffTargetOneHits");
		rEngine.setNumberPlayers(3);
		Player P1 = rEngine.addPlayers("Jackson", "100.000.0.0");
		Player P2 = rEngine.addPlayers("Truong", "100.000.0.0");
		Player P3 = rEngine.addPlayers("Robert", "100.000.0.0");
		P1.setCommands("Thrust", 2, "Dodge", 2);
		P2.setCommands("Thrust", 2, "Duck", 2);
		P3.setCommands("Smash", 2, "Charge", 2);
		Boolean result1 = rEngine.AttackConclusion(P1,P2); //AttackConclusion(PlayerAtk, PlayerDef)
		Boolean result2 = rEngine.AttackConclusion(P2,P3); //AttackConclusion(PlayerAtk, PlayerDef)
		Boolean result3 = rEngine.AttackConclusion(P3,P1); //AttackConclusion(PlayerAtk, PlayerDef)
		assertTrue(result1 == false && P2.getCountWounds() == 0); //No hits done by player one attacking player two
		assertTrue(result2 == true && P3.getCountWounds() == 1); //hits done by player two attacking player three
		assertTrue(result3 == false && P1.getCountWounds() == 0); //No hits done by player three attacking player one
		System.out.println(rEngine.EndRoundMessage());;
	}
    
    @Test
   	public void GameRound3PlayersDiffTargetTwoHits() {
   		System.out.println("@Test(): GameRound3PlayersDiffTargetTwoHits");
   		rEngine.setNumberPlayers(3);
   		Player P1 = rEngine.addPlayers("Jackson", "100.000.0.0");
   		Player P2 = rEngine.addPlayers("Truong", "100.000.0.0");
   		Player P3 = rEngine.addPlayers("Robert", "100.000.0.0");
   		P1.setCommands("Thrust", 2, "Dodge", 2);
   		P2.setCommands("Thrust", 2, "Duck", 2);
   		P3.setCommands("Swing", 2, "Charge", 2);
   		Boolean result1 = rEngine.AttackConclusion(P1,P2); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result2 = rEngine.AttackConclusion(P2,P3); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result3 = rEngine.AttackConclusion(P3,P1); //AttackConclusion(PlayerAtk, PlayerDef)
   		assertTrue(result1 == false && P2.getCountWounds() == 0); //No hits done by player one attacking player two
   		assertTrue(result2 == true && P3.getCountWounds() == 1); //hits done by player two attacking player three
   		assertTrue(result3 == true && P1.getCountWounds() == 1); //hits done by player three attacking player one
   		System.out.println(rEngine.EndRoundMessage());;
   	}
    
    @Test
   	public void GameRound3PlayersDiffTargetThreeHits() {
   		System.out.println("@Test(): GameRound3PlayersDiffTargetThreeHits");
   		rEngine.setNumberPlayers(3);
   		Player P1 = rEngine.addPlayers("Jackson", "100.000.0.0");
   		Player P2 = rEngine.addPlayers("Truong", "100.000.0.0");
   		Player P3 = rEngine.addPlayers("Robert", "100.000.0.0");
   		P1.setCommands("Thrust", 1, "Dodge", 3);
   		P2.setCommands("Thrust", 1, "Duck", 3);
   		P3.setCommands("Swing", 1, "Charge", 3);
   		Boolean result1 = rEngine.AttackConclusion(P1,P2); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result2 = rEngine.AttackConclusion(P2,P3); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result3 = rEngine.AttackConclusion(P3,P1); //AttackConclusion(PlayerAtk, PlayerDef)
   		assertTrue(result1 == true && P2.getCountWounds() == 1); //No hits done by player one attacking player two
   		assertTrue(result2 == true && P3.getCountWounds() == 1); //hits done by player two attacking player three
   		assertTrue(result3 == true && P1.getCountWounds() == 1); //hits done by player three attacking player one
   		System.out.println(rEngine.EndRoundMessage());;
   	}
    
    @Test
   	public void GameRound3PlayersSameTargetNoHits() {
   		System.out.println("@Test(): GameRound3PlayersSameTargetNoHits");
   		rEngine.setNumberPlayers(3);
   		Player P1 = rEngine.addPlayers("Jackson", "100.000.0.0");
   		Player P2 = rEngine.addPlayers("Truong", "100.000.0.0");
   		Player P3 = rEngine.addPlayers("Robert", "100.000.0.0");
   		P1.setCommands("Thrust", 2, "Dodge", 2);
   		P2.setCommands("Thrust", 2, "Duck", 2);
   		P3.setCommands("Thrust", 2, "Charge", 2);
   		Boolean result1 = rEngine.AttackConclusion(P1,P2); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result2 = rEngine.AttackConclusion(P2,P1); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result3 = rEngine.AttackConclusion(P3,P1); //AttackConclusion(PlayerAtk, PlayerDef)
   		assertTrue(result1 == false && P2.getCountWounds() == 0); //No hits done by player one attacking player two
   		assertTrue(result2 == false && P1.getCountWounds() == 0); //No hits done by player two attacking player one
   		assertTrue(result3 == false && P1.getCountWounds() == 0); //No hits done by player three attacking player one
   		System.out.println(rEngine.EndRoundMessage());;
   	}
    
    @Test
   	public void GameRound3PlayersSameTargetOneHits() {
   		System.out.println("@Test(): GameRound3PlayersSameTargetOneHits");
   		rEngine.setNumberPlayers(3);
   		Player P1 = rEngine.addPlayers("Jackson", "100.000.0.0");
   		Player P2 = rEngine.addPlayers("Truong", "100.000.0.0");
   		Player P3 = rEngine.addPlayers("Robert", "100.000.0.0");
   		P1.setCommands("Thrust", 2, "Dodge", 2);
   		P2.setCommands("Swing", 2, "Duck", 2);
   		P3.setCommands("Smash", 2, "Dodge", 2);
   		Boolean result1 = rEngine.AttackConclusion(P1,P2); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result2 = rEngine.AttackConclusion(P2,P1); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result3 = rEngine.AttackConclusion(P3,P1); //AttackConclusion(PlayerAtk, PlayerDef)
   		assertTrue(result1 == false && P2.getCountWounds() == 0); //No hits done by player one attacking player two
   		assertTrue(result2 == true && P1.getCountWounds() == 1); //hits done by player two attacking player one
   		assertTrue(result3 == false && P1.getCountWounds() == 1); //No hits done by player three attacking player one, BUT one existing hit from p2->p1
   		System.out.println(rEngine.EndRoundMessage());;
   	}
    
    @Test
   	public void GameRound3PlayersSameTargetTwoHits() {
   		System.out.println("@Test(): GameRound3PlayersSameTargetTwoHits");
   		rEngine.setNumberPlayers(3);
   		Player P1 = rEngine.addPlayers("Jackson", "100.000.0.0");
   		Player P2 = rEngine.addPlayers("Truong", "100.000.0.0");
   		Player P3 = rEngine.addPlayers("Robert", "100.000.0.0");
   		P1.setCommands("Thrust", 2, "Dodge", 2);
   		P2.setCommands("Swing", 2, "Duck", 2);
   		P3.setCommands("Swing", 2, "Dodge", 2);
   		Boolean result1 = rEngine.AttackConclusion(P1,P2); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result2 = rEngine.AttackConclusion(P2,P1); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result3 = rEngine.AttackConclusion(P3,P1); //AttackConclusion(PlayerAtk, PlayerDef)
   		assertTrue(result1 == false && P2.getCountWounds() == 0); //No hits done by player one attacking player two
   		assertTrue(result2 == true && P1.getCountWounds() == 2); //hits done by player two attacking player one, BUT one existing hit from p3->p1
   		assertTrue(result3 == true && P1.getCountWounds() == 2); //hits done by player three attacking player one, BUT one existing hit from p2->p1
   		System.out.println(rEngine.EndRoundMessage());;
   	}
    
    @Test
   	public void GameRound3PlayersSameTargetAllHits() {
   		System.out.println("@Test(): GameRound3PlayersSameTargetAllHits");
   		rEngine.setNumberPlayers(3);
   		Player P1 = rEngine.addPlayers("Jackson", "100.000.0.0");
   		Player P2 = rEngine.addPlayers("Truong", "100.000.0.0");
   		Player P3 = rEngine.addPlayers("Robert", "100.000.0.0");
   		P1.setCommands("Thrust", 2, "Dodge", 2);
   		P2.setCommands("Swing", 2, "Charge", 2);
   		P3.setCommands("Swing", 2, "Dodge", 2);
   		Boolean result1 = rEngine.AttackConclusion(P1,P2); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result2 = rEngine.AttackConclusion(P2,P1); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result3 = rEngine.AttackConclusion(P3,P1); //AttackConclusion(PlayerAtk, PlayerDef)
   		assertTrue(result1 == true && P2.getCountWounds() == 1); //hits done by player one attacking player two
   		assertTrue(result2 == true && P1.getCountWounds() == 2); //hits done by player two attacking player one, BUT one existing hit from p3->p1
   		assertTrue(result3 == true && P1.getCountWounds() == 2); //hits done by player three attacking player one, BUT one existing hit from p2->p1
   		System.out.println(rEngine.EndRoundMessage());;
   	}
}
