package TDD;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import RulesEngine.Player;
import RulesEngine.RulesEngine;

public class TestRulesEngineTwoPlayers {
	RulesEngine rEngine;
	
	@BeforeClass
    public static void BeforeClass() {
        System.out.println("@BeforeClass: TestRulesEngineTwoPlayers");
    }
    
    @Before
    public void setUp() {
		System.out.println("@Before: TestRulesEngineTwoPlayers");
		rEngine = new RulesEngine();
	}
	
    @After
    public void tearDown () {
		System.out.println("@After(): TestRulesEngineTwoPlayers");
		rEngine = null;
	}
	
    @AfterClass
    public static void afterClass () {
    	 System.out.println("@AfterClass: TestRulesEngineTwoPlayers");
    }
   
	@Test
	public void GameRound2PlayersNoHits() {
		System.out.println("@Test(): GameRound2PlayersNoHits");
		rEngine.setNumberPlayers(2);
		Player P1 = rEngine.addPlayers("Jackson", "100.000.0.0");
		Player P2 = rEngine.addPlayers("Truong", "100.000.0.0");
		P1.setCommands("Thrust", 2, "Duck", 2);
		P2.setCommands("Swing", 2, "Duck", 2);
		Boolean result1 = rEngine.AttackConclusion(P1,P2); //AttackConclusion(PlayerAtk, PlayerDef)
		Boolean result2 = rEngine.AttackConclusion(P2,P1); //AttackConclusion(PlayerAtk, PlayerDef)
		assertTrue(result1 == false && P2.getCountWounds() == 0 ); //No hits done by player one
		assertTrue(result2 == false && P1.getCountWounds() == 0); //No hits done by player two
		System.out.println(rEngine.EndRoundMessage());
	}
	
	@Test
	public void GameRound2PlayersONEHitOnP1() {
		System.out.println("@Test(): GameRound2PlayersONEHitOnP1");
		rEngine.setNumberPlayers(2);
		Player P1 = rEngine.addPlayers("Jackson", "100.000.0.0");
		Player P2 = rEngine.addPlayers("Truong", "100.000.0.0");
		P1.setCommands("Thrust", 3, "Duck", 1);
		P2.setCommands("Smash", 1, "Duck", 3);
		Boolean result1 = rEngine.AttackConclusion(P1,P2); //AttackConclusion(PlayerAtk, PlayerDef)
		Boolean result2 = rEngine.AttackConclusion(P2,P1); //AttackConclusion(PlayerAtk, PlayerDef)
		assertTrue(result1 == false && P2.getCountWounds() == 0); //No hits done by player one
		assertTrue(result2 == true && P1.getCountWounds() == 1); //hits done by player two
		System.out.println(rEngine.EndRoundMessage());;
	}
	
	@Test
	public void GameRound2PlayersONEHitOnP2() {
		System.out.println("@Test(): GameRound2PlayersONEHitOnP2");
		rEngine.setNumberPlayers(2);
		Player P1 = rEngine.addPlayers("Jackson", "100.000.0.0");
		Player P2 = rEngine.addPlayers("Truong", "100.000.0.0");
		P1.setCommands("Smash", 3, "Duck", 1);
		P2.setCommands("Thrust", 1, "Duck", 3);
		Boolean result1 = rEngine.AttackConclusion(P1,P2); //AttackConclusion(PlayerAtk, PlayerDef)
		Boolean result2 = rEngine.AttackConclusion(P2,P1); //AttackConclusion(PlayerAtk, PlayerDef)
		assertTrue(result1 == true && P2.getCountWounds() == 1); //hits done by player one
		assertTrue(result2 == false && P1.getCountWounds() == 0); //No hits done by player two
		System.out.println(rEngine.EndRoundMessage());;
	}
	
	@Test
	public void GameRound2PlayersBothHit() {
		System.out.println("@Test(): GameRound2PlayersBothHit");
		rEngine.setNumberPlayers(2);
		Player P1 = rEngine.addPlayers("Jackson", "100.000.0.0");
		Player P2 = rEngine.addPlayers("Truong", "100.000.0.0");
		P1.setCommands("Smash", 3, "Dodge", 1);
		P2.setCommands("Swing", 1, "Duck", 3);
		Boolean result1 = rEngine.AttackConclusion(P1,P2); //AttackConclusion(PlayerAtk, PlayerDef)
		Boolean result2 = rEngine.AttackConclusion(P2,P1); //AttackConclusion(PlayerAtk, PlayerDef)
		assertTrue(result1 == true && P2.getCountWounds() == 1); //hits done by player one
		assertTrue(result2 == true && P1.getCountWounds() == 1); //hits done by player two
		System.out.println(rEngine.EndRoundMessage());
	}
}
