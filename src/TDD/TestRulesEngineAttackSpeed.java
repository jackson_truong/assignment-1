package TDD;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import RulesEngine.Player;
import RulesEngine.RulesEngine;

public class TestRulesEngineAttackSpeed {
	RulesEngine rEngine;
	
	@BeforeClass
    public static void BeforeClass() {
        System.out.println("@BeforeClass: TestRulesEngineAttackSpeed");
    }
    
    @Before
    public void setUp() {
		System.out.println("@Before: TestRulesEngineAttackSpeed");
		rEngine = new RulesEngine();
	}
	
    @After
    public void tearDown () {
		System.out.println("@After(): TestRulesEngineAttackSpeed");
		rEngine = null;
	}
	
    @AfterClass
    public static void afterClass () {
    	 System.out.println("@AfterClass: TestRulesEngineAttackSpeed");
    }
	
    //Testing Atking player with A:1 D:3
    
	@Test
	public void ProcessAttackSpeed1() {
		rEngine.setNumberPlayers(2);
		System.out.println("@Test(): ProcessAttackSpeed1");
		Player P1 = new Player("John", "100.000.0.0");
		Player P2 = new Player("Jack", "211.111.1.1");
		P1.setCommands("Thrust", 1, "Duck", 3);
		P2.setCommands("Thrust", 1, "Duck", 3);
		Boolean result = rEngine.ProcessAttackSpeed(P1,P2); //ProcessAttackSpeed(PlayerAtk, PlayerDef)
		assertTrue(result == true);
	}
	
	@Test
	public void ProcessAttackSpeed2() {
		rEngine.setNumberPlayers(2);
		System.out.println("@Test(): ProcessAttackSpeed2");
		Player P1 = new Player("John", "100.000.0.0");
		Player P2 = new Player("Jack", "211.111.1.1");
		P1.setCommands("Thrust", 1, "Duck", 3);
		P2.setCommands("Thrust", 2, "Duck", 2);
		Boolean result = rEngine.ProcessAttackSpeed(P1,P2); //ProcessAttackSpeed(PlayerAtk, PlayerDef)
		assertTrue(result == true);
	}
	
	@Test
	public void ProcessAttackSpeed3() {
		rEngine.setNumberPlayers(2);
		System.out.println("@Test(): ProcessAttackSpeed3");
		Player P1 = new Player("John", "100.000.0.0");
		Player P2 = new Player("Jack", "211.111.1.1");
		P1.setCommands("Thrust", 1, "Duck", 3);
		P2.setCommands("Thrust", 3, "Duck", 1);
		Boolean result = rEngine.ProcessAttackSpeed(P1,P2); //ProcessAttackSpeed(PlayerAtk, PlayerDef)
		assertTrue(result == false);
	}
	
    //Testing Atking player with A:2 D:2
    
	@Test
	public void ProcessAttackSpeed4() {
		rEngine.setNumberPlayers(2);
		System.out.println("@Test(): ProcessAttackSpeed4");
		Player P1 = new Player("John", "100.000.0.0");
		Player P2 = new Player("Jack", "211.111.1.1");
		P1.setCommands("Thrust", 2, "Duck", 2);
		P2.setCommands("Thrust", 1, "Duck", 3);
		Boolean result = rEngine.ProcessAttackSpeed(P1,P2); //ProcessAttackSpeed(PlayerAtk, PlayerDef)
		assertTrue(result == true);
	}
	
	@Test
	public void ProcessAttackSpeed5() {
		rEngine.setNumberPlayers(2);
		System.out.println("@Test(): ProcessAttackSpeed5");
		Player P1 = new Player("John", "100.000.0.0");
		Player P2 = new Player("Jack", "211.111.1.1");
		P1.setCommands("Thrust", 2, "Duck", 2);
		P2.setCommands("Thrust", 2, "Duck", 2);
		Boolean result = rEngine.ProcessAttackSpeed(P1,P2); //ProcessAttackSpeed(PlayerAtk, PlayerDef)
		assertTrue(result == false);
	}
	
	@Test
	public void ProcessAttackSpeed6() {
		rEngine.setNumberPlayers(2);
		System.out.println("@Test(): ProcessAttackSpeed6");
		Player P1 = new Player("John", "100.000.0.0");
		Player P2 = new Player("Jack", "211.111.1.1");
		P1.setCommands("Thrust", 2, "Duck", 2);
		P2.setCommands("Thrust", 3, "Duck", 1);
		Boolean result = rEngine.ProcessAttackSpeed(P1,P2); //ProcessAttackSpeed(PlayerAtk, PlayerDef)
		assertTrue(result == false);
	}
	
    //Testing Atking player with A:3 D:1
    
	@Test
	public void ProcessAttackSpeed7() {
		rEngine.setNumberPlayers(2);
		System.out.println("@Test(): ProcessAttackSpeed7");
		Player P1 = new Player("John", "100.000.0.0");
		Player P2 = new Player("Jack", "211.111.1.1");
		P1.setCommands("Thrust", 3, "Duck", 1);
		P2.setCommands("Thrust", 1, "Duck", 3);
		Boolean result = rEngine.ProcessAttackSpeed(P1,P2); //ProcessAttackSpeed(PlayerAtk, PlayerDef)
		assertTrue(result == false);
	}
	
	@Test
	public void ProcessAttackSpeed8() {
		rEngine.setNumberPlayers(2);
		System.out.println("@Test(): ProcessAttackSpeed8");
		Player P1 = new Player("John", "100.000.0.0");
		Player P2 = new Player("Jack", "211.111.1.1");
		P1.setCommands("Thrust", 3, "Duck", 1);
		P2.setCommands("Thrust", 2, "Duck", 2);
		Boolean result = rEngine.ProcessAttackSpeed(P1,P2); //ProcessAttackSpeed(PlayerAtk, PlayerDef)
		assertTrue(result == false);
	}
	
	@Test
	public void ProcessAttackSpeed9() {
		rEngine.setNumberPlayers(2);
		System.out.println("@Test(): ProcessAttackSpeed9");
		Player P1 = new Player("John", "100.000.0.0");
		Player P2 = new Player("Jack", "211.111.1.1");
		P1.setCommands("Thrust", 3, "Duck", 1);
		P2.setCommands("Thrust", 3, "Duck", 1);
		Boolean result = rEngine.ProcessAttackSpeed(P1,P2); //ProcessAttackSpeed(PlayerAtk, PlayerDef)
		assertTrue(result == false);
	}

}
