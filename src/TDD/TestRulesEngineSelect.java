package TDD;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import RulesEngine.RulesEngine;

public class TestRulesEngineSelect {
	RulesEngine rEngine;
	
	@BeforeClass
    public static void BeforeClass() {
        System.out.println("@BeforeClass: TestRulesEngineSelect");
    }
    
    @Before
    public void setUp() {
		System.out.println("@Before: TestRulesEngineSelect");
		rEngine = new RulesEngine();
	}
	
    @After
    public void tearDown () {
		System.out.println("@After(): TestRulesEngineSelect");
		rEngine = null;
	}
	
    @AfterClass
    public static void afterClass () {
    	 System.out.println("@AfterClass: TestRulesEngineSelect");
    }
	
    
    // Select Command tests
	@Test
	public void ProcessSelectCommandValidity1() {
		System.out.println("@Test(): ProcessSelectCommandValidity1");
		boolean result = false;
		if(	rEngine.SelectCommandValidity("Thrust", 1, "Charge", 3) &&
			rEngine.SelectCommandValidity("Thrust", 2, "Charge", 2) &&
			rEngine.SelectCommandValidity("Thrust", 3, "Charge", 1)){
			result = true;
		}
		assertEquals(result, true);
	}
	
	@Test
	public void ProcessSelectCommandValidity2() {
		System.out.println("@Test(): ProcessSelectCommandValidity2");
		boolean result = false;
		if(	rEngine.SelectCommandValidity("Thrust", 1, "Dodge", 3) &&
			rEngine.SelectCommandValidity("Thrust", 2, "Dodge", 2) &&
			rEngine.SelectCommandValidity("Thrust", 3, "Dodge", 1)){
			result = true;
		}
		assertEquals(result, true);
	}
	
	@Test
	public void ProcessSelectCommandValidity3() {
		System.out.println("@Test(): ProcessSelectCommandValidity3");
		boolean result = false;
		if(	rEngine.SelectCommandValidity("Thrust", 1, "Duck", 3) &&
			rEngine.SelectCommandValidity("Thrust", 2, "Duck", 2) &&
			rEngine.SelectCommandValidity("Thrust", 3, "Duck", 1)){
			result = true;
		}
		assertEquals(result, true);
	}
	
	@Test
	public void ProcessSelectCommandValidity4() {
		System.out.println("@Test(): ProcessSelectCommandValidity4");
		boolean result = false;
		if(	rEngine.SelectCommandValidity("Swing", 1, "Charge", 3) &&
			rEngine.SelectCommandValidity("Swing", 2, "Charge", 2) &&
			rEngine.SelectCommandValidity("Swing", 3, "Charge", 1)){
			result = true;
		}
		assertEquals(result, true);
	}
	
	@Test
	public void ProcessSelectCommandValidity5() {
		System.out.println("@Test(): ProcessSelectCommandValidity5");
		boolean result = false;
		if(	rEngine.SelectCommandValidity("Swing", 1, "Dodge", 3) &&
			rEngine.SelectCommandValidity("Swing", 2, "Dodge", 2) &&
			rEngine.SelectCommandValidity("Swing", 3, "Dodge", 1)){
			result = true;
		}
		assertEquals(result, true);
	}
	
	@Test
	public void ProcessSelectCommandValidity6() {
		System.out.println("@Test(): ProcessSelectCommandValidity6");
		boolean result = false;
		if(	rEngine.SelectCommandValidity("Swing", 1, "Duck", 3) &&
			rEngine.SelectCommandValidity("Swing", 2, "Duck", 2) &&
			rEngine.SelectCommandValidity("Swing", 3, "Duck", 1)){
			result = true;
		}
		assertEquals(result, true);
	}
	
	@Test
	public void ProcessSelectCommandValidity7() {
		System.out.println("@Test(): ProcessSelectCommandValidity7");
		boolean result = false;
		if(	rEngine.SelectCommandValidity("Smash", 1, "Charge", 3) &&
			rEngine.SelectCommandValidity("Smash", 2, "Charge", 2) &&
			rEngine.SelectCommandValidity("Smash", 3, "Charge", 1)){
			result = true;
		}
		assertEquals(result, true);
	}
	
	@Test
	public void ProcessSelectCommandValidity8() {
		System.out.println("@Test(): ProcessSelectCommandValidity8");
		boolean result = false;
		if(	rEngine.SelectCommandValidity("Smash", 1, "Dodge", 3) &&
			rEngine.SelectCommandValidity("Smash", 2, "Dodge", 2) &&
			rEngine.SelectCommandValidity("Smash", 3, "Dodge", 1)){
			result = true;
		}
		assertEquals(result, true);
	}
	
	@Test
	public void ProcessSelectCommandValidity9() {
		System.out.println("@Test(): ProcessSelectCommandValidity9");
		boolean result = false;
		if(	rEngine.SelectCommandValidity("Smash", 1, "Duck", 3) &&
			rEngine.SelectCommandValidity("Smash", 2, "Duck", 2) &&
			rEngine.SelectCommandValidity("Smash", 3, "Duck", 1)){
			result = true;
		}
		assertEquals(result, true);
	}
	
	@Test
	public void ProcessSelectCommandValidity10() {
		System.out.println("@Test(): ProcessSelectCommandValidity10");
		boolean result = false;
		if(	rEngine.SelectCommandValidity("RUN", 1, "Duck", 3) &&
			rEngine.SelectCommandValidity("Smash", 2, "Duck", 2) &&
			rEngine.SelectCommandValidity("Smash", 3, "Duck", 1)){
			result = true;
		}
		assertEquals(result, false);
	}
	
	@Test
	public void ProcessSelectCommandValidity11() {
		System.out.println("@Test(): ProcessSelectCommandValidity11");
		boolean result = false;
		if(	rEngine.SelectCommandValidity("Smash", 1, "Duck", 3) &&
			rEngine.SelectCommandValidity("Smash", 2, "CRY", 2) &&
			rEngine.SelectCommandValidity("Smash", 3, "Duck", 1)){
			result = true;
		}
		assertEquals(result, false);
	}
	
	@Test
	public void ProcessSelectCommandValidity12() {
		System.out.println("@Test(): ProcessSelectCommandValidity12");
		boolean result = false;
		if(	rEngine.SelectCommandValidity("Smash", 1, "Duck", 3) &&
			rEngine.SelectCommandValidity("Smash", 4, "Duck", 2) &&
			rEngine.SelectCommandValidity("Smash", 3, "Duck", 1)){
			result = true;
		}
		assertEquals(result, false);
	}
	
	@Test
	public void ProcessSelectCommandValidity13() {
		System.out.println("@Test(): ProcessSelectCommandValidity13");
		boolean result = false;
		if(	rEngine.SelectCommandValidity("Smash", 0, "Duck", 3) &&
			rEngine.SelectCommandValidity("Smash", 2, "Duck", 2) &&
			rEngine.SelectCommandValidity("Smash", 3, "Duck", 1)){
			result = true;
		}
		assertEquals(result, false);
	}
	
	//select targets with no players joined in the game
	@Test
	public void ProcessSelectTargetValidityNoPlayers1() {
		System.out.println("@Test(): ProcessSelectTargetValidityNoPlayers1");
		assertTrue(null == rEngine.SelectTarget("Bob"));
	}
	
	@Test
	public void ProcessSelectTargetValidityNoPlayers2() {
		System.out.println("@Test(): ProcessSelectTargetValidityNoPlayers2");
		assertTrue(null == rEngine.SelectTarget("John"));
	}
	
	//select targets with addplayer() 
	@Test
	public void ProcessSelectTargetValidityPlayers1() {
		rEngine.setNumberPlayers(2);
		System.out.println("@Test(): ProcessSelectTargetValidityPlayers1");
		rEngine.addPlayers("Jackson", "100.000.0.0");
		rEngine.addPlayers("Jack", "211.111.1.1");
		assertTrue("Jackson" == rEngine.SelectTarget("Jackson"));
	}
	
	@Test
	public void ProcessSelectTargetValidityPlayers2() {
		rEngine.setNumberPlayers(4);
		System.out.println("@Test(): ProcessSelectTargetValidityPlayers2");
		rEngine.addPlayers("Jackson", "100.000.0.0");
		rEngine.addPlayers("Jack", "211.111.1.1");
		rEngine.addPlayers("Truong", "100.000.0.0");
		rEngine.addPlayers("Robert", "211.111.1.1");
		assertTrue("Robert" == rEngine.SelectTarget("Robert"));
	}
	
	@Test
	public void ProcessSelectTargetValidityPlayers3() {
		rEngine.setNumberPlayers(3);
		System.out.println("@Test(): ProcessSelectTargetValidityPlayers3");
		rEngine.addPlayers("Jackson", "100.000.0.0");
		rEngine.addPlayers("Jack", "211.111.1.1");
		rEngine.addPlayers("Truong", "100.000.0.0");
		assertTrue(null == rEngine.SelectTarget("John"));
	}
	
	@Test
	public void ProcessSelectTargetValidityPlayers4() {
		rEngine.setNumberPlayers(4);
		System.out.println("@Test(): ProcessSelectTargetValidityPlayers4");
		rEngine.addPlayers("Jackson", "100.000.0.0");
		rEngine.addPlayers("Jack", "211.111.1.1");
		rEngine.addPlayers("Truong", "100.000.0.0");
		rEngine.addPlayers("Robert", "211.111.1.1");
		assertTrue(null == rEngine.SelectTarget("Henry"));
	}
}
