package TDD;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import RulesEngine.Player;
import RulesEngine.RulesEngine;

public class TestRulesEngineTarget {
	RulesEngine rEngine;
	
	@BeforeClass
    public static void BeforeClass() {
        System.out.println("@BeforeClass: TestRulesEngineTarget");
    }
    
    @Before
    public void setUp() {
		System.out.println("@Before: TestRulesEngineTarget");
		rEngine = new RulesEngine();
	}
	
    @After
    public void tearDown () {
		System.out.println("@After(): TestRulesEngineTarget");
		rEngine = null;
	}
	
    @AfterClass
    public static void afterClass () {
    	 System.out.println("@AfterClass: TestRulesEngineTarget");
    }
    
    //THRUST for the next 3 tests
	@Test
	public void ProcessTarget1() { 
		rEngine.setNumberPlayers(2);
		System.out.println("@Test(): ProcessTarget1");
		Player P1 = new Player("John", "100.000.0.0");
		Player P2 = new Player("Jack", "211.111.1.1");
		P1.setCommands("Thrust", 1, "Duck", 3);
		P2.setCommands("Thrust", 1, "Charge", 3);
		Boolean result = rEngine.ProcessTarget(P1,P2); //ProcessTarget(PlayerAtk, PlayerDef), Testing Thrust -> Charge
		assertTrue(result == true);
	}
	
	@Test
	public void ProcessTarget2() { 
		rEngine.setNumberPlayers(2);
		System.out.println("@Test(): ProcessTarget2");
		Player P1 = new Player("John", "100.000.0.0");
		Player P2 = new Player("Jack", "211.111.1.1");
		P1.setCommands("Thrust", 1, "Duck", 3);
		P2.setCommands("Thrust", 1, "Dodge", 3);
		Boolean result = rEngine.ProcessTarget(P1,P2); //ProcessTarget(PlayerAtk, PlayerDef), Testing Thrust -> Dodge
		assertTrue(result == false);
	}
	
	@Test
	public void ProcessTarget3() {
		rEngine.setNumberPlayers(2);
		System.out.println("@Test(): ProcessTarget3");
		Player P1 = new Player("John", "100.000.0.0");
		Player P2 = new Player("Jack", "211.111.1.1");
		P1.setCommands("Thrust", 1, "Duck", 3);
		P2.setCommands("Thrust", 1, "Duck", 3);
		Boolean result = rEngine.ProcessTarget(P1,P2); //ProcessTarget(PlayerAtk, PlayerDef), Testing Thrust -> Duck
		assertTrue(result == false);
	}
	
    //SMASH for the next 3 tests
	@Test
	public void ProcessTarget4() {
		rEngine.setNumberPlayers(2);
		System.out.println("@Test(): ProcessTarget4");
		Player P1 = new Player("John", "100.000.0.0");
		Player P2 = new Player("Jack", "211.111.1.1");
		P1.setCommands("Smash", 1, "Duck", 3);
		P2.setCommands("Thrust", 1, "Charge", 3);
		Boolean result = rEngine.ProcessTarget(P1,P2); //ProcessTarget(PlayerAtk, PlayerDef), Testing Smash -> Charge
		assertTrue(result == false);
	}
	
	@Test
	public void ProcessTarget5() {
		rEngine.setNumberPlayers(2);
		System.out.println("@Test(): ProcessTarget5");
		Player P1 = new Player("John", "100.000.0.0");
		Player P2 = new Player("Jack", "211.111.1.1");
		P1.setCommands("Smash", 1, "Duck", 3);
		P2.setCommands("Thrust", 1, "Dodge", 3);
		Boolean result = rEngine.ProcessTarget(P1,P2); //ProcessTarget(PlayerAtk, PlayerDef), Testing Smash -> Dodge
		assertTrue(result == false);
	}
	
	@Test
	public void ProcessTarget6() {
		rEngine.setNumberPlayers(2);
		System.out.println("@Test(): ProcessTarget6");
		Player P1 = new Player("John", "100.000.0.0");
		Player P2 = new Player("Jack", "211.111.1.1");
		P1.setCommands("Smash", 1, "Duck", 3);
		P2.setCommands("Thrust", 1, "Duck", 3);
		Boolean result = rEngine.ProcessTarget(P1,P2); //ProcessTarget(PlayerAtk, PlayerDef), Testing Smash -> Duck
		assertTrue(result == true);
	}
	
    //Swing for the next 3 tests
	@Test
	public void ProcessTarget7() {
		rEngine.setNumberPlayers(2);
		System.out.println("@Test(): ProcessTarget7");
		Player P1 = new Player("John", "100.000.0.0");
		Player P2 = new Player("Jack", "211.111.1.1");
		P1.setCommands("Swing", 1, "Duck", 3);
		P2.setCommands("Thrust", 1, "Charge", 3);
		Boolean result = rEngine.ProcessTarget(P1,P2); //ProcessTarget(PlayerAtk, PlayerDef), Testing Swing -> Charge
		assertTrue(result == false);
	}
	
	@Test
	public void ProcessTarget8() {
		rEngine.setNumberPlayers(2);
		System.out.println("@Test(): ProcessTarget8");
		Player P1 = new Player("John", "100.000.0.0");
		Player P2 = new Player("Jack", "211.111.1.1");
		P1.setCommands("Swing", 1, "Duck", 3);
		P2.setCommands("Thrust", 1, "Dodge", 3);
		Boolean result = rEngine.ProcessTarget(P1,P2); //ProcessTarget(PlayerAtk, PlayerDef), Testing Swing -> Dodge
		assertTrue(result == true);
	}
	
	@Test
	public void ProcessTarget9() {
		rEngine.setNumberPlayers(2);
		System.out.println("@Test(): ProcessTarget9");
		Player P1 = new Player("John", "100.000.0.0");
		Player P2 = new Player("Jack", "211.111.1.1");
		P1.setCommands("Swing", 1, "Duck", 3);
		P2.setCommands("Thrust", 1, "Duck", 3);
		Boolean result = rEngine.ProcessTarget(P1,P2); //ProcessTarget(PlayerAtk, PlayerDef), Testing Swing -> Duck
		assertTrue(result == false);
	}
	
	@Test
	public void ProcessTarget10() {
		rEngine.setNumberPlayers(2);
		System.out.println("@Test(): ProcessTarget9");
		Player P1 = new Player("John", "100.000.0.0");
		Player P2 = new Player("Jack", "211.111.1.1");
		P1.setCommands("Cry", 1, "Duck", 3);
		P2.setCommands("Thrust", 1, "Duck", 3);
		Boolean result = rEngine.ProcessTarget(P1,P2); //ProcessTarget(PlayerAtk, PlayerDef), Testing Swing -> Duck
		assertTrue(result == false);
	}

}
