package TDD;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import RulesEngine.Player;
import RulesEngine.RulesEngine;

public class TestRulesEngineAttackDirection {
	RulesEngine rEngine;
	
	@BeforeClass
    public static void BeforeClass() {
        System.out.println("@BeforeClass: TestRulesEngineAttackDirection");
    }
    
    @Before
    public void setUp() {
		System.out.println("@Before: TestRulesEngineAttackDirection");
		rEngine = new RulesEngine();
	}
	
    @After
    public void tearDown () {
		System.out.println("@After(): TestRulesEngineAttackDirection");
		rEngine = null;
	}
	
    @AfterClass
    public static void afterClass () {
    	 System.out.println("@AfterClass: TestRulesEngineAttackDirection");
    }
    
	@Test
	public void ProcessAttackDirection1() {
		rEngine.setNumberPlayers(1);
		System.out.println("@Test(): ProcessAttackDirection1");
		Player P1 = new Player("John", "100.000.0.0");
		P1.setCommands("Thrust", 2, "Duck", 2);
		String result1 = rEngine.ProcessAttackDirection(P1,1); //ProcessAttackDirection(PlayerAtk, Dice)
		String result2 = rEngine.ProcessAttackDirection(P1,2); //ProcessAttackDirection(PlayerAtk, Dice)
		assertTrue(result1 == "Thrust" && result2 == "Thrust");
	}
	
	@Test
	public void ProcessAttackDirection2() {
		rEngine.setNumberPlayers(1);
		System.out.println("@Test(): ProcessAttackDirection2");
		Player P1 = new Player("John", "100.000.0.0");
		P1.setCommands("Thrust", 2, "Duck", 2);
		String result1 = rEngine.ProcessAttackDirection(P1,3); //ProcessAttackDirection(PlayerAtk, Dice)
		String result2 = rEngine.ProcessAttackDirection(P1,4); //ProcessAttackDirection(PlayerAtk, Dice)
		assertTrue(result1 == "Smash" && result2 == "Smash");
	}

	@Test
	public void ProcessAttackDirection3() {
		rEngine.setNumberPlayers(1);
		System.out.println("@Test(): ProcessAttackDirection3");
		Player P1 = new Player("John", "100.000.0.0");
		P1.setCommands("Thrust", 2, "Duck", 2);
		String result1 = rEngine.ProcessAttackDirection(P1,5); //ProcessAttackDirection(PlayerAtk, Dice)
		String result2 = rEngine.ProcessAttackDirection(P1,6); //ProcessAttackDirection(PlayerAtk, Dice)
		assertTrue(result1 == "Swing" && result2 == "Swing");
	}
	
	@Test
	public void ProcessAttackDirection4() {
		rEngine.setNumberPlayers(1);
		System.out.println("@Test(): ProcessAttackDirection4");
		Player P1 = new Player("John", "100.000.0.0");
		P1.setCommands("Swing", 2, "Duck", 2);
		String result1 = rEngine.ProcessAttackDirection(P1,1); //ProcessAttackDirection(PlayerAtk, Dice)
		String result2 = rEngine.ProcessAttackDirection(P1,2); //ProcessAttackDirection(PlayerAtk, Dice)
		assertTrue(result1 == "Swing" && result2 == "Swing");
	}
	
	@Test
	public void ProcessAttackDirection5() {
		rEngine.setNumberPlayers(1);
		System.out.println("@Test(): ProcessAttackDirection5");
		Player P1 = new Player("John", "100.000.0.0");
		P1.setCommands("Swing", 2, "Duck", 2);
		String result1 = rEngine.ProcessAttackDirection(P1,3); //ProcessAttackDirection(PlayerAtk, Dice)
		String result2 = rEngine.ProcessAttackDirection(P1,4); //ProcessAttackDirection(PlayerAtk, Dice)
		assertTrue(result1 == "Thrust" && result2 == "Thrust");
	}

	@Test
	public void ProcessAttackDirection6() {
		rEngine.setNumberPlayers(1);
		System.out.println("@Test(): ProcessAttackDirection6");
		Player P1 = new Player("John", "100.000.0.0");
		P1.setCommands("Swing", 2, "Duck", 2);
		String result1 = rEngine.ProcessAttackDirection(P1,5); //ProcessAttackDirection(PlayerAtk, Dice)
		String result2 = rEngine.ProcessAttackDirection(P1,6); //ProcessAttackDirection(PlayerAtk, Dice)
		assertTrue(result1 == "Smash" && result2 == "Smash");
	}
	
	@Test
	public void ProcessAttackDirection7() {
		rEngine.setNumberPlayers(1);
		System.out.println("@Test(): ProcessAttackDirection7");
		Player P1 = new Player("John", "100.000.0.0");
		P1.setCommands("Smash", 2, "Duck", 2);
		String result1 = rEngine.ProcessAttackDirection(P1,1); //ProcessAttackSpeed(PlayerAtk, Dice)
		String result2 = rEngine.ProcessAttackDirection(P1,2); //ProcessAttackSpeed(PlayerAtk, Dice)
		assertTrue(result1 == "Smash" && result2 == "Smash");
	}
	
	@Test
	public void ProcessAttackDirection8() {
		rEngine.setNumberPlayers(1);
		System.out.println("@Test(): ProcessAttackDirection8");
		Player P1 = new Player("John", "100.000.0.0");
		P1.setCommands("Smash", 2, "Duck", 2);
		String result1 = rEngine.ProcessAttackDirection(P1,3); //ProcessAttackDirection(PlayerAtk, Dice)
		String result2 = rEngine.ProcessAttackDirection(P1,4); //ProcessAttackDirection(PlayerAtk, Dice)
		assertTrue(result1 == "Swing" && result2 == "Swing");
	}

	@Test
	public void ProcessAttackDirection9() {
		rEngine.setNumberPlayers(1);
		System.out.println("@Test(): ProcessAttackDirection9");
		Player P1 = new Player("John", "100.000.0.0");
		P1.setCommands("Smash", 2, "Duck", 2);
		String result1 = rEngine.ProcessAttackDirection(P1,5); //ProcessAttackDirection(PlayerAtk, Dice)
		String result2 = rEngine.ProcessAttackDirection(P1,6); //ProcessAttackDirection(PlayerAtk, Dice)
		assertTrue(result1 == "Thrust" && result2 == "Thrust");
	}
	
	@Test
	public void ProcessAttackDirection10() {
		rEngine.setNumberPlayers(1);
		System.out.println("@Test(): ProcessAttackDirection9");
		Player P1 = new Player("John", "100.000.0.0");
		P1.setCommands("RUN", 2, "Duck", 2);
		String result1 = rEngine.ProcessAttackDirection(P1,5); //ProcessAttackDirection(PlayerAtk, Dice)
		String result2 = rEngine.ProcessAttackDirection(P1,6); //ProcessAttackDirection(PlayerAtk, Dice)
		assertTrue(result1 == null && result2 == null);
	}
}
