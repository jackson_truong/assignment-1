package TDD;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import RulesEngine.Player;
import RulesEngine.RulesEngine;

public class TestRulesEngineFourPlayers {
RulesEngine rEngine;
	
	@BeforeClass
    public static void BeforeClass() {
        System.out.println("@BeforeClass: TestRulesEngineFourPlayers");
    }
    
    @Before
    public void setUp() {
		System.out.println("@Before: TestRulesEngineFourPlayers");
		rEngine = new RulesEngine();
	}
	
    @After
    public void tearDown () {
		System.out.println("@After(): TestRulesEngineFourPlayers");
		rEngine = null;
	}
	
    @AfterClass
    public static void afterClass () {
    	 System.out.println("@AfterClass: TestRulesEngineFourPlayers");
    }
	
    @Test
	public void GameRound4PlayersDiffTargetNoHits() {
		System.out.println("@Test(): GameRound4PlayersDiffTargetNoHits");
		rEngine.setNumberPlayers(4);
		Player P1 = rEngine.addPlayers("Jackson", "100.000.0.0");
		Player P2 = rEngine.addPlayers("Truong", "100.000.0.0");
		Player P3 = rEngine.addPlayers("John", "100.000.0.0");
		Player P4 = rEngine.addPlayers("Snow", "100.000.0.0");
		P1.setCommands("Thrust", 2, "Dodge", 2);
		P2.setCommands("Swing", 2, "Dodge", 2);
		P3.setCommands("Smash", 2, "Charge", 2);
		P4.setCommands("Smash", 2, "Dodge", 2);
		Boolean result1 = rEngine.AttackConclusion(P1,P4); //AttackConclusion(PlayerAtk, PlayerDef)
		Boolean result2 = rEngine.AttackConclusion(P2,P3); //AttackConclusion(PlayerAtk, PlayerDef)
		Boolean result3 = rEngine.AttackConclusion(P3,P2); //AttackConclusion(PlayerAtk, PlayerDef)
		Boolean result4 = rEngine.AttackConclusion(P4,P1); //AttackConclusion(PlayerAtk, PlayerDef)
		assertTrue(result1 == false && P4.getCountWounds() == 0); //No hits done by player one attacking player four
		assertTrue(result2 == false && P3.getCountWounds() == 0); //No hits done by player two attacking player three
		assertTrue(result3 == false && P2.getCountWounds() == 0); //No hits done by player three attacking player two
		assertTrue(result4 == false && P1.getCountWounds() == 0); //No hits done by player Four attacking player one
		System.out.println(rEngine.EndRoundMessage());;
	}
    
    @Test
	public void GameRound4PlayersDiffTargetOneHits() {
		System.out.println("@Test(): GameRound4PlayersDiffTargetOneHits");
		rEngine.setNumberPlayers(4);
		Player P1 = rEngine.addPlayers("Jackson", "100.000.0.0");
		Player P2 = rEngine.addPlayers("Truong", "100.000.0.0");
		Player P3 = rEngine.addPlayers("John", "100.000.0.0");
		Player P4 = rEngine.addPlayers("Snow", "100.000.0.0");
		P1.setCommands("Thrust", 2, "Dodge", 2);
		P2.setCommands("Swing", 2, "Dodge", 2);
		P3.setCommands("Smash", 2, "Charge", 2);
		P4.setCommands("Smash", 2, "Charge", 2);
		Boolean result1 = rEngine.AttackConclusion(P1,P4); //AttackConclusion(PlayerAtk, PlayerDef)
		Boolean result2 = rEngine.AttackConclusion(P2,P3); //AttackConclusion(PlayerAtk, PlayerDef)
		Boolean result3 = rEngine.AttackConclusion(P3,P2); //AttackConclusion(PlayerAtk, PlayerDef)
		Boolean result4 = rEngine.AttackConclusion(P4,P1); //AttackConclusion(PlayerAtk, PlayerDef)
		assertTrue(result1 == true && P4.getCountWounds() == 1); //hits done by player one attacking player four
		assertTrue(result2 == false && P3.getCountWounds() == 0); //No hits done by player two attacking player three
		assertTrue(result3 == false && P2.getCountWounds() == 0); //No hits done by player three attacking player two
		assertTrue(result4 == false && P1.getCountWounds() == 0); //No hits done by player Four attacking player one
		System.out.println(rEngine.EndRoundMessage());;
	}
    
    @Test
   	public void GameRound4PlayersDiffTargetTwoHits() {
   		System.out.println("@Test(): GameRound4PlayersDiffTargetTwoHits");
   		rEngine.setNumberPlayers(4);
   		Player P1 = rEngine.addPlayers("Jackson", "100.000.0.0");
   		Player P2 = rEngine.addPlayers("Truong", "100.000.0.0");
   		Player P3 = rEngine.addPlayers("John", "100.000.0.0");
   		Player P4 = rEngine.addPlayers("Snow", "100.000.0.0");
   		P1.setCommands("Thrust", 2, "Dodge", 2);
   		P2.setCommands("Swing", 2, "Dodge", 2);
   		P3.setCommands("Swing", 2, "Charge", 2);
   		P4.setCommands("Smash", 2, "Charge", 2);
   		Boolean result1 = rEngine.AttackConclusion(P1,P4); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result2 = rEngine.AttackConclusion(P2,P3); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result3 = rEngine.AttackConclusion(P3,P2); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result4 = rEngine.AttackConclusion(P4,P1); //AttackConclusion(PlayerAtk, PlayerDef)
   		assertTrue(result1 == true && P4.getCountWounds() == 1); //hits done by player one attacking player four
   		assertTrue(result2 == false && P3.getCountWounds() == 0); //No hits done by player two attacking player three
   		assertTrue(result3 == true && P2.getCountWounds() == 1); //hits done by player three attacking player two
   		assertTrue(result4 == false && P1.getCountWounds() == 0); //No hits done by player Four attacking player one
   		System.out.println(rEngine.EndRoundMessage());;
   	}
    
    @Test
   	public void GameRound4PlayersDiffTargetThreeHits() {
   		System.out.println("@Test(): GameRound4PlayersDiffTargetThreeHits");
   		rEngine.setNumberPlayers(4);
   		Player P1 = rEngine.addPlayers("Jackson", "100.000.0.0");
   		Player P2 = rEngine.addPlayers("Truong", "100.000.0.0");
   		Player P3 = rEngine.addPlayers("John", "100.000.0.0");
   		Player P4 = rEngine.addPlayers("Snow", "100.000.0.0");
   		P1.setCommands("Thrust", 2, "Dodge", 2);
   		P2.setCommands("Swing", 2, "Dodge", 2);
   		P3.setCommands("Smash", 1, "Charge", 3);
   		P4.setCommands("Smash", 2, "Charge", 2);
   		Boolean result1 = rEngine.AttackConclusion(P1,P4); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result2 = rEngine.AttackConclusion(P2,P3); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result3 = rEngine.AttackConclusion(P3,P2); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result4 = rEngine.AttackConclusion(P4,P1); //AttackConclusion(PlayerAtk, PlayerDef)
   		assertTrue(result1 == true && P4.getCountWounds() == 1); //hits done by player one attacking player four
   		assertTrue(result2 == true && P3.getCountWounds() == 1); //hits done by player two attacking player three
   		assertTrue(result3 == true && P2.getCountWounds() == 1); //hits done by player three attacking player two
   		assertTrue(result4 == false && P1.getCountWounds() == 0); //No hits done by player Four attacking player one
   		System.out.println(rEngine.EndRoundMessage());;
   	}
    
    @Test
   	public void GameRound4PlayersDiffTargetFourHits() {
   		System.out.println("@Test(): GameRound4PlayersDiffTargetFourHits");
   		rEngine.setNumberPlayers(4);
   		Player P1 = rEngine.addPlayers("Jackson", "100.000.0.0");
   		Player P2 = rEngine.addPlayers("Truong", "100.000.0.0");
   		Player P3 = rEngine.addPlayers("John", "100.000.0.0");
   		Player P4 = rEngine.addPlayers("Snow", "100.000.0.0");
   		P1.setCommands("Thrust", 2, "Duck", 2);
   		P2.setCommands("Swing", 2, "Dodge", 2);
   		P3.setCommands("Smash", 1, "Charge", 3);
   		P4.setCommands("Smash", 2, "Charge", 2);
   		Boolean result1 = rEngine.AttackConclusion(P1,P4); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result2 = rEngine.AttackConclusion(P2,P3); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result3 = rEngine.AttackConclusion(P3,P2); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result4 = rEngine.AttackConclusion(P4,P1); //AttackConclusion(PlayerAtk, PlayerDef)
   		assertTrue(result1 == true && P4.getCountWounds() == 1); //hits done by player one attacking player four
   		assertTrue(result2 == true && P3.getCountWounds() == 1); //No hits done by player two attacking player three
   		assertTrue(result3 == true && P2.getCountWounds() == 1); //hits done by player three attacking player two
   		assertTrue(result4 == true && P1.getCountWounds() == 1); //hits done by player Four attacking player one
   		System.out.println(rEngine.EndRoundMessage());;
   	}
    
    @Test
   	public void GameRound4PlayersSameTargetTwoPlayersHits1() {
   		System.out.println("@Test(): GameRound4PlayersSameTargetTwoPlayersHits1");
   		rEngine.setNumberPlayers(4);
   		Player P1 = rEngine.addPlayers("Jackson", "100.000.0.0");
   		Player P2 = rEngine.addPlayers("Truong", "100.000.0.0");
   		Player P3 = rEngine.addPlayers("John", "100.000.0.0");
   		Player P4 = rEngine.addPlayers("Snow", "100.000.0.0");
   		P1.setCommands("Thrust", 2, "Duck", 2);
   		P2.setCommands("Swing", 2, "Dodge", 2);
   		P3.setCommands("Smash", 1, "Charge", 3);
   		P4.setCommands("Smash", 2, "Charge", 2);
   		Boolean result1 = rEngine.AttackConclusion(P1,P4); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result2 = rEngine.AttackConclusion(P2,P3); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result3 = rEngine.AttackConclusion(P3,P1); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result4 = rEngine.AttackConclusion(P4,P1); //AttackConclusion(PlayerAtk, PlayerDef)
   		assertTrue(result1 == true && P4.getCountWounds() == 1); //hits done by player one attacking player four
   		assertTrue(result2 == true && P3.getCountWounds() == 1); //hits done by player two attacking player three
   		assertTrue(result3 == true && P1.getCountWounds() == 2); //hits done by player three attacking player one
   		assertTrue(result4 == true && P1.getCountWounds() == 2); //hits done by player Four attacking player one
   		System.out.println(rEngine.EndRoundMessage());;
   	}
    
    @Test
   	public void GameRound4PlayersSameTargetTwoPlayersHits2() {
   		System.out.println("@Test(): GameRound4PlayersSameTargetTwoPlayersHits2");
   		rEngine.setNumberPlayers(4);
   		Player P1 = rEngine.addPlayers("Jackson", "100.000.0.0");
   		Player P2 = rEngine.addPlayers("Truong", "100.000.0.0");
   		Player P3 = rEngine.addPlayers("John", "100.000.0.0");
   		Player P4 = rEngine.addPlayers("Snow", "100.000.0.0");
   		P1.setCommands("Thrust", 3, "Duck", 1);
   		P2.setCommands("Swing", 2, "Dodge", 2);
   		P3.setCommands("Swing", 1, "Charge", 3);
   		P4.setCommands("Smash", 2, "Charge", 2);
   		Boolean result1 = rEngine.AttackConclusion(P1,P3); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result2 = rEngine.AttackConclusion(P2,P4); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result3 = rEngine.AttackConclusion(P3,P1); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result4 = rEngine.AttackConclusion(P4,P1); //AttackConclusion(PlayerAtk, PlayerDef)
   		assertTrue(result1 == true && P3.getCountWounds() == 1); //hits done by player one attacking player three
   		assertTrue(result2 == false && P4.getCountWounds() == 0); //No hits done by player two attacking player four
   		assertTrue(result3 == false && P1.getCountWounds() == 1); //No hits done by player three attacking player two, but one from p4->p1
   		assertTrue(result4 == true && P1.getCountWounds() == 1); //No hits done by player Four attacking player one
   		System.out.println(rEngine.EndRoundMessage());;
   	}
    
    @Test
   	public void GameRound4PlayersSameTargetThreePlayersNoHits() {
   		System.out.println("@Test(): GameRound4PlayersSameTargetThreePlayersNoHits");
   		rEngine.setNumberPlayers(4);
   		Player P1 = rEngine.addPlayers("Jackson", "100.000.0.0");
   		Player P2 = rEngine.addPlayers("Truong", "100.000.0.0");
   		Player P3 = rEngine.addPlayers("John", "100.000.0.0");
   		Player P4 = rEngine.addPlayers("Snow", "100.000.0.0");
   		P1.setCommands("Thrust", 2, "Duck", 2);
   		P2.setCommands("Swing", 2, "Dodge", 2);
   		P3.setCommands("Swing", 2, "Dodge", 2);
   		P4.setCommands("Swing", 2, "Charge", 2);
   		Boolean result1 = rEngine.AttackConclusion(P1,P3); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result2 = rEngine.AttackConclusion(P2,P1); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result3 = rEngine.AttackConclusion(P3,P1); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result4 = rEngine.AttackConclusion(P4,P1); //AttackConclusion(PlayerAtk, PlayerDef)
   		assertTrue(result1 == false && P3.getCountWounds() == 0); //No hits done by player one attacking player three
   		assertTrue(result2 == false && P1.getCountWounds() == 0); //No hits done by player two attacking player one
   		assertTrue(result3 == false && P1.getCountWounds() == 0); //No hits done by player three attacking player one
   		assertTrue(result4 == false && P1.getCountWounds() == 0); //No hits done by player Four attacking player one
   		System.out.println(rEngine.EndRoundMessage());;
   	}
    
    @Test
   	public void GameRound4PlayersSameTargetThreePlayersOneHits() {
   		System.out.println("@Test(): GameRound4PlayersSameTargetThreePlayersOneHits");
   		rEngine.setNumberPlayers(4);
   		Player P1 = rEngine.addPlayers("Jackson", "100.000.0.0");
   		Player P2 = rEngine.addPlayers("Truong", "100.000.0.0");
   		Player P3 = rEngine.addPlayers("John", "100.000.0.0");
   		Player P4 = rEngine.addPlayers("Snow", "100.000.0.0");
   		P1.setCommands("Thrust", 2, "Duck", 2);
   		P2.setCommands("Swing", 2, "Dodge", 2);
   		P3.setCommands("Smash", 2, "Dodge", 2);
   		P4.setCommands("Swing", 2, "Charge", 2);
   		Boolean result1 = rEngine.AttackConclusion(P1,P3); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result2 = rEngine.AttackConclusion(P2,P1); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result3 = rEngine.AttackConclusion(P3,P1); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result4 = rEngine.AttackConclusion(P4,P1); //AttackConclusion(PlayerAtk, PlayerDef)
   		assertTrue(result1 == false && P3.getCountWounds() == 0); //No hits done by player one attacking player three
   		assertTrue(result2 == false && P1.getCountWounds() == 1); //No hits done by player two attacking player one, but 1 from p3->p1
   		assertTrue(result3 == true && P1.getCountWounds() == 1); //hits done by player three attacking player one
   		assertTrue(result4 == false && P1.getCountWounds() == 1); //No hits done by player Four attacking player one, but 1 from p3->p1
   		System.out.println(rEngine.EndRoundMessage());;
   	}
    
    @Test
   	public void GameRound4PlayersSameTargetThreePlayersTwoHits() {
   		System.out.println("@Test(): GameRound4PlayersSameTargetThreePlayersTwoHits");
   		rEngine.setNumberPlayers(4);
   		Player P1 = rEngine.addPlayers("Jackson", "100.000.0.0");
   		Player P2 = rEngine.addPlayers("Truong", "100.000.0.0");
   		Player P3 = rEngine.addPlayers("John", "100.000.0.0");
   		Player P4 = rEngine.addPlayers("Snow", "100.000.0.0");
   		P1.setCommands("Thrust", 2, "Duck", 2);
   		P2.setCommands("Swing", 2, "Dodge", 2);
   		P3.setCommands("Smash", 2, "Dodge", 2);
   		P4.setCommands("Smash", 2, "Charge", 2);
   		Boolean result1 = rEngine.AttackConclusion(P1,P3); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result2 = rEngine.AttackConclusion(P2,P1); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result3 = rEngine.AttackConclusion(P3,P1); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result4 = rEngine.AttackConclusion(P4,P1); //AttackConclusion(PlayerAtk, PlayerDef)
   		assertTrue(result1 == false && P3.getCountWounds() == 0); //No hits done by player one attacking player three
   		assertTrue(result2 == false && P1.getCountWounds() == 2); //No hits done by player two attacking player one, but 1 from p3->p1 and p4->p1
   		assertTrue(result3 == true && P1.getCountWounds() == 2); //hits done by player three attacking player one, AND p4->p1
   		assertTrue(result4 == true && P1.getCountWounds() == 2); //hits done by player Four attacking player one, AND 1 from p3->p1
   		System.out.println(rEngine.EndRoundMessage());;
   	}
    
    @Test
   	public void GameRound4PlayersSameTargetThreePlayersThreeHits() {
   		System.out.println("@Test(): GameRound4PlayersSameTargetThreePlayersThreeHits");
   		rEngine.setNumberPlayers(4);
   		Player P1 = rEngine.addPlayers("Jackson", "100.000.0.0");
   		Player P2 = rEngine.addPlayers("Truong", "100.000.0.0");
   		Player P3 = rEngine.addPlayers("John", "100.000.0.0");
   		Player P4 = rEngine.addPlayers("Snow", "100.000.0.0");
   		P1.setCommands("Thrust", 2, "Duck", 2);
   		P2.setCommands("Smash", 2, "Dodge", 2);
   		P3.setCommands("Smash", 2, "Dodge", 2);
   		P4.setCommands("Smash", 2, "Charge", 2);
   		Boolean result1 = rEngine.AttackConclusion(P1,P3); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result2 = rEngine.AttackConclusion(P2,P1); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result3 = rEngine.AttackConclusion(P3,P1); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result4 = rEngine.AttackConclusion(P4,P1); //AttackConclusion(PlayerAtk, PlayerDef)
   		assertTrue(result1 == false && P3.getCountWounds() == 0); //No hits done by player one attacking player three
   		assertTrue(result2 == true && P1.getCountWounds() == 3); //hits done by player two attacking player one, AND 2 from p3->p1 and p4->p1
   		assertTrue(result3 == true && P1.getCountWounds() == 3); //hits done by player three attacking player one, AND 2 from p2->p1 and p4->p1
   		assertTrue(result4 == true && P1.getCountWounds() == 3); //hits done by player Four attacking player one, AND 2 from p3->p1 and p2->p1
   		System.out.println(rEngine.EndRoundMessage());;
   	}
    
    @Test
   	public void GameRound4PlayersSameTargetThreePlayersAllHits() {
   		System.out.println("@Test(): GameRound4PlayersSameTargetThreePlayersAllHits");
   		rEngine.setNumberPlayers(4);
   		Player P1 = rEngine.addPlayers("Jackson", "100.000.0.0");
   		Player P2 = rEngine.addPlayers("Truong", "100.000.0.0");
   		Player P3 = rEngine.addPlayers("John", "100.000.0.0");
   		Player P4 = rEngine.addPlayers("Snow", "100.000.0.0");
   		P1.setCommands("Swing", 2, "Duck", 2);
   		P2.setCommands("Smash", 2, "Dodge", 2);
   		P3.setCommands("Smash", 2, "Dodge", 2);
   		P4.setCommands("Smash", 2, "Charge", 2);
   		Boolean result1 = rEngine.AttackConclusion(P1,P3); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result2 = rEngine.AttackConclusion(P2,P1); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result3 = rEngine.AttackConclusion(P3,P1); //AttackConclusion(PlayerAtk, PlayerDef)
   		Boolean result4 = rEngine.AttackConclusion(P4,P1); //AttackConclusion(PlayerAtk, PlayerDef)
   		assertTrue(result1 == true && P3.getCountWounds() == 1); //hits done by player one attacking player three
   		assertTrue(result2 == true && P1.getCountWounds() == 3); //hits done by player two attacking player one, AND 2 from p3->p1 and p4->p1
   		assertTrue(result3 == true && P1.getCountWounds() == 3); //hits done by player three attacking player one, AND 2 from p2->p1 and p4->p1
   		assertTrue(result4 == true && P1.getCountWounds() == 3); //hits done by player Four attacking player one, AND 2 from p3->p1 and p2->p1
   		System.out.println(rEngine.EndRoundMessage());;
   	}
}
