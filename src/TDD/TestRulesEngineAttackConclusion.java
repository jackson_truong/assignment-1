package TDD;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import RulesEngine.Player;
import RulesEngine.RulesEngine;

public class TestRulesEngineAttackConclusion {
	RulesEngine rEngine;
	
	@BeforeClass
    public static void BeforeClass() {
        System.out.println("@BeforeClass: TestRulesEngineAttackConclusion");
    }
    
    @Before
    public void setUp() {
		System.out.println("@Before: TestRulesEngineAttackConclusion");
		rEngine = new RulesEngine();
	}
	
    @After
    public void tearDown () {
		System.out.println("@After(): TestRulesEngineAttackConclusion");
		rEngine = null;
	}
	
    @AfterClass
    public static void afterClass () {
    	 System.out.println("@AfterClass: TestRulesEngineAttackConclusion");
    }
	
	@Test
	public void AttackConclusion1() {
		rEngine.setNumberPlayers(2);
		System.out.println("@Test(): AttackConclusion1");
		Player P1 = new Player("John", "100.000.0.0");
		Player P2 = new Player("Jack", "211.111.1.1");
		P1.setCommands("Thrust", 1, "Duck", 3);
		P2.setCommands("Thrust", 1, "Charge", 3);
		Boolean result = rEngine.AttackConclusion(P1,P2); //AttackConclusion(PlayerAtk, PlayerDef), both valid
		assertTrue(result == true);
	}
	
	@Test
	public void AttackConclusion2() {
		rEngine.setNumberPlayers(2);
		System.out.println("@Test(): AttackConclusion2");
		Player P1 = new Player("John", "100.000.0.0");
		Player P2 = new Player("Jack", "211.111.1.1");
		P1.setCommands("Smash", 3, "Duck", 1);
		P2.setCommands("Thrust", 3, "Duck", 1);
		Boolean result = rEngine.AttackConclusion(P1,P2); //AttackConclusion(PlayerAtk, PlayerDef), only attack valid 
		assertTrue(result == true);
	}
	
	@Test
	public void AttackConclusion3() {
		rEngine.setNumberPlayers(2);
		System.out.println("@Test(): AttackConclusion3");
		Player P1 = new Player("John", "100.000.0.0");
		Player P2 = new Player("Jack", "211.111.1.1");
		P1.setCommands("Thrust", 2, "Duck", 2);
		P2.setCommands("Thrust", 2, "Duck", 2);
		Boolean result = rEngine.AttackConclusion(P1,P2); //AttackConclusion(PlayerAtk, PlayerDef), none valid
		assertTrue(result == false);
	}
	
	@Test
	public void AttackConclusion4() {
		rEngine.setNumberPlayers(2);
		System.out.println("@Test(): AttackConclusion4");
		Player P1 = new Player("John", "100.000.0.0");
		Player P2 = new Player("Jack", "211.111.1.1");
		P1.setCommands("Thrust", 1, "Duck", 3);
		P2.setCommands("Thrust", 1, "Duck", 3);
		Boolean result = rEngine.AttackConclusion(P1,P2); //AttackConclusion(PlayerAtk, PlayerDef), only speed valid
		assertTrue(result == true);
	}

}