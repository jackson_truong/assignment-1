package RulesEngine;

import java.util.ArrayList;
import java.util.List;

public class RulesEngine {
	
	private final String[] attacks = {"Thrust", "Smash", "Swing"};
	private final String[] defenses = {"Charge", "Dodge", "Duck"};
	
	private int NumPlayers = 0;
		
	private List<Player> players = new ArrayList<Player>();
	
	public void setNumberPlayers(int i) {
		NumPlayers = i;
	}
	
	public Player addPlayers(String name, String IP){
		Player p = new Player(name,IP);
		if(players.size() < NumPlayers){
			players.add(p);
			return p;
		}
		return null;
	}
	
	public List<Player> getPlayers(){
		return players;
	}
	
	public boolean SelectCommandValidity(String Atk, int AtkSpeed, String Def, int DefSpeed){
		boolean validity = true;
		
		if(validity == true){
			for(int i = 0; i < attacks.length; i++){
				if(attacks[i] == Atk){
					validity = true;
					break;
				}
				else{
					validity = false;
				}
			}
		}
		
		if(validity == true){
			for(int i = 0; i < defenses.length; i++){
				if(defenses[i] == Def){
					validity = true;
					break;
				}
				else{
					validity = false;
				}
			}
		}
		
		if(validity == true){
			if( 1 <= AtkSpeed && AtkSpeed <= 3){
				if( 1 <= DefSpeed && DefSpeed <= 3){
					if((AtkSpeed+DefSpeed) >= 4 ){
						validity = true;
					}
					else{
						validity = false;
					}
				}
				else{
					validity = false;
				}
			}
			else{
				validity = false;
			}
		}
		
		return validity;
	}

	public String SelectTarget(String PlayerName) {
		String ValidPlayerName = null;
		for(int i = 0; i < players.size(); i++){
			if(players.get(i).getName() == PlayerName){
				ValidPlayerName = PlayerName;
				break;
			}
			else{
				ValidPlayerName = null;
			}
		}
		return ValidPlayerName;
	}

	public int DiceRoll(){
		return 1 + (int)(Math.random() * 6); 
	}
	
	public Boolean ProcessAttackSpeed(Player p1, Player p2) {
		if(p1.getAttackSpeed() < p2.getDefenseSpeed()){
			return true;
		}
		return false;
	}
	
	public String ProcessAttackDirection(Player p1, int DiceRoll) {
		String Attack = p1.getAttack();
		switch (Attack) {
			case "Thrust": 
				if(DiceRoll == 1 || DiceRoll == 2){
					Attack = "Thrust";
				}
				else if(DiceRoll == 3 || DiceRoll == 4){
					Attack = "Smash";
				}
				else if(DiceRoll == 5 || DiceRoll == 6){
					Attack = "Swing";
				}
				break;
			case "Swing":  
				if(DiceRoll == 1 || DiceRoll == 2){
					Attack = "Swing";
				}
				else if(DiceRoll == 3 || DiceRoll == 4){
					Attack = "Thrust";
				}
				else if(DiceRoll == 5 || DiceRoll == 6){
					Attack = "Smash";
				}
				break;
			case "Smash":  
				if(DiceRoll == 1 || DiceRoll == 2){
					Attack = "Smash";
				}
				else if(DiceRoll == 3 || DiceRoll == 4){
					Attack = "Swing";
				}
				else if(DiceRoll == 5 || DiceRoll == 6){
					Attack = "Thrust";
				}
				break;
			default: 
				Attack = null;
				break;
		}
		return Attack;
	}
	
	public Boolean ProcessTarget(Player p1, Player p2) {
		String Attack = p1.getAttack(); 
		String Defense = p2.getDefense();
		
		boolean Hit = false;
		
		switch (Attack) {
			case "Thrust": 
				if(Defense == "Charge"){
					Hit = true;
				}
				break;
			case "Swing":  
				if(Defense == "Dodge"){
					Hit = true;
				}
				break;
			case "Smash":  
				if(Defense == "Duck"){
					Hit = true;
				}
				break;
			default: 
				Hit = false;
				break;
		}
		
		return Hit;
	}

	public Boolean AttackConclusion(Player p1, Player p2) {
		if(ProcessAttackSpeed(p1,p2) || ProcessTarget(p1,p2)){
			p2.AddWound();
			return true;
		}
		return false;
	}

	public String EndRoundMessage() {
		String CurrentString = "";
		for(int i = 0; i < players.size(); i++){
			if(players.get(i).getName() != null){
				CurrentString += players.get(i).getName()+" has "+players.get(i).getCountWounds()+" wounds\n";
			}
		}
		if(CurrentString == ""){
			return null;
		}
		else{
			return CurrentString;
		}
	}
}
