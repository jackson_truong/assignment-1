package RulesEngine;

public class Player {
	
	private String Pname;
	private String Pip;
	
	private int Wounds;
	@SuppressWarnings("unused")
	private boolean ActionPerformed;
	private Player Ptarget;
	
	//Commands
	private String Patk;
	private int PatkSpeed;
	private String Pdef;
	private int PdefSpeed;
	
	//Get function to return the player name
	public String getName(){
		return this.Pname;
	}
	
	public String getIP(){
		return this.Pip;
	}
	
	public int getCountWounds(){
		return this.Wounds;
	}
	
	//Get function to return the player's target
	public Player getTarget(){
		return this.Ptarget;
	}
	
	//Get function to return the player name
	public String getAttack(){
		return this.Patk;
	}
	
	//Get function to return the player's target
	public int getAttackSpeed(){
		return this.PatkSpeed;
	}
	
	//Get function to return the player name
	public String getDefense(){
		return this.Pdef;
	}
	
	//Get function to return the player's target
	public int getDefenseSpeed(){
		return this.PdefSpeed;
	}
	
	//Set function to set the players commands
	public void setCommands(String Atk, int AtkSpeed, String Def, int DefSpeed){
		this.Patk = Atk;
		this.PatkSpeed = AtkSpeed;
		this.Pdef = Def;
		this.PdefSpeed = DefSpeed;
	}
	
	//Set function to set the players commands
	public void setTarget(Player T){
		this.Ptarget = T;
	}

	
	public Player(){
		//Nothing happens here
	}
	
	public Player(String PlayerName, String PlayerIPaddress){
		this.Pname = PlayerName;
		this.Pip = PlayerIPaddress;
	}

	public void AddWound() {
		this.Wounds++;
	}
	
}
