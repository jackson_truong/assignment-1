package Server;

import java.net.*;
import java.util.Date;

import org.apache.log4j.*;

import java.io.*;

public class Server {
	
	public static Logger logger = Logger.getLogger(Server.class.getName());
	
	public static void sendMessage(String str,Socket clientSocket){
		PrintWriter out;
		try {
			out = new PrintWriter(clientSocket.getOutputStream(), true);
    		out.println(str);
		} catch (IOException e) {
			//ERROR
		}
	}

	public static void startup(int Port){
		
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(5050);
            logger.info("Server port Started: "+serverSocket.getInetAddress()+": "+serverSocket.getLocalPort());
        } catch (IOException e) {
            System.exit(1);
        }
        
        while(true)
        {
        	Socket clientSocket = null;
        	try{
	        	clientSocket = serverSocket.accept();
        	}
        	catch(IOException e){
        		logger.info("Client ["+clientSocket.getInetAddress()+":"+clientSocket.getLocalPort()+"]"+" attempted to log in on "+ new Date());
        		System.out.println("No Connections");
        	}
        	if(clientSocket.isConnected()){
        		logger.info("Client connected: "+clientSocket.getInetAddress()+": "+clientSocket.getLocalPort()+" - "+ new Date());
        		sendMessage("Client ["+clientSocket.getInetAddress()+":"+clientSocket.getLocalPort()+"]"+": Logged in on "+ new Date(), clientSocket);       		
        	}
        }
    }
	
	public static void main(String[] argv) {
		BasicConfigurator.configure();
		PropertyConfigurator.configure("./properties/log4j.properties");
		startup(5050);
	}

	public void stop() {
		System.exit(1);
	}
}