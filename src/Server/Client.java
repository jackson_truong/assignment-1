package Server;

import java.io.*;
import java.net.*;

public class Client {
	
	private Thread thread = null;
	private ClientThread client = null;

	public boolean connect(String IPAdresss, int Port) {
		Socket clientSocket;
		try {
			clientSocket = new Socket(IPAdresss, Port);
			if(clientSocket.isConnected()){
				BufferedReader input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			    String answer = input.readLine();
			    System.out.println(answer);
			    return true;
			}
		} catch (IOException e) {
			System.err.println("Could not connect");
			return false;
		}
		return false;
	}
}
