package Server;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ClientTest {
	Client client;
	
	@BeforeClass
    public static void BeforeClass() {
        //System.out.println("@BeforeClass: ClientTest");
    }
    
    @Before
    public void setUp() {
		//System.out.println("@Before: ClientTest");
		client = new Client();
	}
	
    @After
    public void tearDown () {
		//System.out.println("@After(): ClientTest");
		client = null;
	}
	
    @AfterClass
    public static void afterClass () {
    	 //System.out.println("@AfterClass: ClientTest");
    }
    
	@Test
	public void TestOneConnection() {
		System.out.println("@test(): TestOneConnection");
		assertTrue(client.connect("localhost",5050));
	}
	
	@Test
	public void TestMultiConnection() {
		System.out.println("@test(): TestMultiConnection");
		Client client1 = new Client();
		Client client2 = new Client();
		Client client3 = new Client();
		Client client4 = new Client();
		assertTrue(client1.connect("localhost",5050));
		assertTrue(client2.connect("localhost",5050));
		assertTrue(client3.connect("localhost",5050));
		assertTrue(client4.connect("localhost",5050));
	}
}
