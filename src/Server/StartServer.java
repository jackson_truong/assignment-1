package Server;

import java.io.Console;
import java.io.IOException;
import java.util.Scanner;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

@SuppressWarnings("unused")
public class StartServer {
	
	private static Boolean done = Boolean.FALSE;
	private static Boolean started = Boolean.FALSE;

	private static Scanner sc = new Scanner(System.in);
	
	private static Server server = new Server();
	
	static Logger logger = Logger.getLogger(StartServer.class.getName());
	
	public static void main(String[] argv) {
		BasicConfigurator.configure();
		//PropertyConfigurator.configure(Config.ServerConfig);
		System.out.println("Starting server ...");

		do {
			System.out.println("startup | shutdown");
			String input = sc.nextLine();
			
			if (input.equalsIgnoreCase("STARTUP") || input.equalsIgnoreCase("START") && !started)
			{
				System.out.println("Starting server ...");
				server.startup(5050);
							
			}
			
			if (input.equalsIgnoreCase("SHUTDOWN") && started)
			{
				System.out.println("Shutting server down ...");
				server.stop();
					
			}			
		} while (!done);

		System.out.println("Exiting ...");
		System.exit(1);
	}
}
