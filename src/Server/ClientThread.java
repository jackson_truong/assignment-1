package Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class ClientThread extends Thread {
	private Socket socket = null;
	private Client client = null;
	private BufferedReader streamIn = null;
	private boolean done = false;
	
	public ClientThread(Client client, Socket socket) {  
		this.client = client;
		this.socket = socket;
		this.start();
	}
}
